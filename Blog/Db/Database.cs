﻿using System.Web;
using Blog.Models;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;

namespace Blog.Db
{
    public static class Database
    {
        private const string SessionKey="Blog.Db.Database.Sessionkey";
        private static ISessionFactory _sessionFactory;

        public static ISession Session => (ISession) HttpContext.Current.Items[SessionKey];

        public static void Configure()
        {
            var config = new Configuration();

            //automatically look at web config to get conn string 
            config.Configure();

            //mapping models to db
            var mapper = new ModelMapper();
            mapper.AddMapping<UserMap>();
            mapper.AddMapping<RoleMap>();

            config.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

            _sessionFactory = config.BuildSessionFactory();
        }

        public static void OpenSession()
        {
            HttpContext.Current.Items[SessionKey] = _sessionFactory.OpenSession();
        }

        public static void CloseSession()
        {
            var session = HttpContext.Current.Items[SessionKey] as ISession;

            if (session != null)
            {
                session.Close();
            }

            HttpContext.Current.Items.Remove(SessionKey);

        }
    }
}