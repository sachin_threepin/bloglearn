﻿using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels
{
    public class AuthLogin
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}