﻿using System.Web.Mvc;
using Blog.Infrastructure;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    [ActiveTab("Posts")]
    public class PostsController : Controller
    {
        // GET: Admin/Posts
        public ActionResult Index()
        {
            return View();
        }
    }
}