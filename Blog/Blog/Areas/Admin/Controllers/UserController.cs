﻿using System.Web.Mvc;
using Blog.Infrastructure;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    [ActiveTab("User")]
    public class UserController : Controller
    {
        // GET: Admin/User
        public ActionResult Index()
        {
            return View();
        }
    }
}