﻿using System.Web.Mvc;
using System.Web.Security;
using Blog.ViewModels;

namespace Blog.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
            return View(new AuthLogin());
        }

        [HttpPost]
        public ActionResult Login(AuthLogin form,string returnurl)
        {
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            else
            {
                FormsAuthentication.SetAuthCookie(form.Username, true);
                if (!string.IsNullOrWhiteSpace(returnurl))
                {
                    return Redirect(returnurl);
                }
                return RedirectToRoute("Home");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("Home");
        }
    }
}