﻿using System.Linq;
using System.Web;
using Blog.Db;
using Blog.Models;
using NHibernate.Linq;

namespace Blog
{
    public static class Auth
    {
        private const string UserKey = "Blog.Auth.UserKey";

        public static User user
        {
            get
            {
                if (!HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    return null;
                }
                else
                {
                    var user = HttpContext.Current.Items[UserKey] as User;
                    if (user == null)
                    {
                        user = Database.Session.Query<User>().FirstOrDefault(u => u.UserName == HttpContext.Current.User.Identity.Name);

                        if (user == null)
                        {
                            return null;
                        }
                        else
                        {
                            HttpContext.Current.Items[UserKey] = user;
                        }
                    }
                    return user;
                }
            }
        }
    }
}