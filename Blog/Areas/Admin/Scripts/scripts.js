﻿$(document).ready(function() {
    $('a[data-post]').click(function (e) {
        e.preventDefault();

        var $this = $(this);
        var msg = $this.data('post');
        

        
        if (msg && !confirm(msg)) {
            return;
        } else {
            $('<form>').attr('method', 'post')
                .attr('action', $this.attr('href')).appendTo(document.body)
                .append($('<input type="hidden" name="">').attr("name",$('#antitoken').attr('name')).val('#antitoken').find('input').val())
                .submit();
        }
    });


})