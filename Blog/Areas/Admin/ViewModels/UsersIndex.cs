﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Blog.Models;

namespace Blog.Areas.Admin.ViewModels
{
    public class RoleCheckBox
    {
        public int Id { get; set; }
        public bool IsChecked { get; set; }
        public string Name { get; set; }
    }
    public class UsersIndex
    {
        public IEnumerable<User> Users { get; set; }

    }

    public class NewUser
    {
        [Required,MaxLength(128)]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required,MaxLength(256)]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }

        public IList<RoleCheckBox> Roles { get; set; }
    }

    public class EditUser
    {
        [Required, MaxLength(128)]
        public string UserName { get; set; }
        
        [Required, MaxLength(256)]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
    }

    public class UserResetPassword
    {
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}