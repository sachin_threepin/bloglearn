﻿using System.Linq;
using System.Web.Mvc;
using Blog.Areas.Admin.ViewModels;
using Blog.Db;
using Blog.Infrastructure;
using Blog.Models;
using NHibernate.Linq;

namespace Blog.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    [ActiveTab("User")]
    public class UserController : Controller
    {
        // GET: Admin/User
        public ActionResult Index()
        {
            return View(new UsersIndex
            {
                Users = Database.Session.Query<User>().ToList()
            });
        }

        public ActionResult New()
        {
            return View(new NewUser
            {
                Roles = Database.Session.Query<Role>().Select(r=>new RoleCheckBox
                {
                        Id = r.Id,
                        Name = r.Name,
                        IsChecked = false
                }).ToList()
            });
        }

        [HttpPost,ValidateAntiForgeryToken]
        public ActionResult New(NewUser form)
        {
            if (Database.Session.Query<User>().Any(user => user.UserName == form.UserName))
            {
                ModelState.AddModelError("UserName", "UserName must be Unique");
            }
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            else
            {
                var user = new User
                {
                    EmailId = form.EmailId,
                    UserName = form.UserName

                };
                user.SetPassword(form.Password);
                Database.Session.Save(user);
                return RedirectToAction("Index");
            }
        }

        public ActionResult Edit(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(new EditUser
                {
                    UserName = user.UserName,
                    EmailId = user.EmailId
                });
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(int id, EditUser form)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            if (Database.Session.Query<User>().Any(u => u.UserName == form.UserName && u.Id != id))
            {
                ModelState.AddModelError("UserName", "UserName already taken!");
            }
            if (Database.Session.Query<User>().Any(u => u.UserName == form.UserName && u.Id == id))
            {
                ModelState.AddModelError("UserName", "Already Same Username");
            }
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            else
            {
                user.UserName = form.UserName;
                user.EmailId = form.EmailId;
                Database.Session.Update(user);
                return RedirectToAction("Index");
            }
        }

        public ActionResult ResetPassword(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(new UserResetPassword
                {
                    UserName = user.UserName
                });
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult ResetPassword(int id, UserResetPassword form)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            form.UserName = user.UserName;

            if (!ModelState.IsValid)
            {
                return View(form);
            }
            user.SetPassword(form.Password);
            Database.Session.Update(user);
            return RedirectToAction("Index");
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            var user = Database.Session.Load<User>(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            else
            {
                Database.Session.Delete(user);
                return RedirectToAction("Index");
            }
        }
    }
}