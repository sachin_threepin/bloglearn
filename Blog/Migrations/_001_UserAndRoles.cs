﻿using System.Data;
using FluentMigrator;

namespace Blog.Migrations
{
    [Migration(1)]
    public class _001_UserAndRoles : Migration
    {
        public override void Up()
        {
            Create.Table("tblusers")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Username").AsString(128).NotNullable()
                .WithColumn("EmailId").AsString(256).NotNullable()
                .WithColumn("Password_Hash").AsString(128).NotNullable();

            Create.Table("tblroles").WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Name").AsString(127).Nullable();

            Create.Table("tbluser_roles").
                WithColumn("UserId").AsInt32().ForeignKey("tblusers", "Id").OnDelete(Rule.Cascade)
                .WithColumn("RolesId").AsInt32().ForeignKey("tblroles", "Id").OnDelete(Rule.Cascade);

            //.WithColumn("EmailId").AsCustom("VARCHAR(245)")
        }

        public override void Down()
        {
            Delete.Table("tblusers");
            Delete.Table("tblroles");
            Delete.Table("tbluser_roles");
        }
    }
}