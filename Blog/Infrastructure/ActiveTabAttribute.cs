﻿using System;
using System.Web.Mvc;

namespace Blog.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ActiveTabAttribute : ActionFilterAttribute
    {
        private readonly string _activeTab;
        public ActiveTabAttribute(string activeTab)
        {
            _activeTab = activeTab;
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.Controller.ViewBag.ActiveTab = _activeTab;
        }

    }
}