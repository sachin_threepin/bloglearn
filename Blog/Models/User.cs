﻿using System.Collections.Generic;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Blog.Models
{
    public class User
    {
        public const int WorkFactor = 13;
       public virtual int Id { get; set; }
       public virtual string UserName { get; set; }
       public virtual string EmailId { get; set; }
       public virtual string Password_Hash { get; set; }
        public virtual IList<Role> Roles { get; set; }

        public User()
        {
            Roles = new List<Role>();
        }

        public virtual void SetPassword(string password)
        {
            Password_Hash = BCrypt.Net.BCrypt.HashPassword(password,WorkFactor);
        }

        public virtual bool CheckPassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, Password_Hash);
        }

        public static void FakeHash()
        {
            BCrypt.Net.BCrypt.HashPassword("", WorkFactor);
        }
    }

    public class UserMap : ClassMapping<User>
    {
        public UserMap()
        {
            //tells which table in db this class is mapped to
            Table("tblusers");

            //tells primary key     and auto increment
            Id(x => x.Id,x=>x.Generator(Generators.Identity));

            Property(x => x.UserName, x => x.NotNullable(true));
            Property(x => x.EmailId, x => x.NotNullable(true));

            Property(x => x.Password_Hash, x =>
            {
                x.Column("Password_Hash");
                x.NotNullable(true);
            });

            Bag(x => x.Roles, x =>
            {
                x.Table("tbluser_roles");
                x.Key(k => k.Column("UserId"));
            },x=>x.ManyToMany(k=>k.Column("RolesId")));
        }
    }
}