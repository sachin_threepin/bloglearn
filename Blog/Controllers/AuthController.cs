﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Blog.Db;
using Blog.Models;
using Blog.ViewModels;
using NHibernate.Linq;

namespace Blog.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
            return View(new AuthLogin());
        }

        [HttpPost]
        public ActionResult Login(AuthLogin form, string returnurl)
        {

            var user = Database.Session.Query<User>().FirstOrDefault(u => u.UserName == form.Username);
            if (user == null || !user.CheckPassword(form.Password))
            {
                Blog.Models.User.FakeHash();
                ModelState.AddModelError("UserName", "UserName or Password Incorrect");
            }
            if (!ModelState.IsValid)
            {
                return View(form);
            }
            FormsAuthentication.SetAuthCookie(user.UserName, true);
            if (!string.IsNullOrWhiteSpace(returnurl))
            {
                return Redirect(returnurl);
            }
            return RedirectToRoute("Home");
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToRoute("Home");
        }
    }
}